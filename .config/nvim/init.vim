" Plugin Manager {{{

  " Autoinstall vim-plug {{{
    if empty(glob('~/.local/share/nvim/site/autoload/plug.vim'))
      silent !curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs
        \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
      autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
    endif
  " }}}
  
  " Install Plugins {{{
    call plug#begin('~/.vim/plugged')
      Plug 'drewtempelmeyer/palenight.vim'
      Plug 'itchyny/lightline.vim'
      Plug 'preservim/nerdtree'
      Plug 'junegunn/fzf', { 'dir': '~/.fzf', 'do': './install --all' }
      Plug 'junegunn/fzf.vim'
      Plug 'neoclide/coc.nvim', {'branch': 'release'}
      Plug 'sonph/onehalf', {'rtp': 'vim/'}
    call plug#end()
  " }}}

" }}}


" General {{{

  set encoding=utf-8                                                " the encoding displayed.
  set fileencoding=utf-8                                            " the encoding written to file.
  let mapleader = ','                                               " The default leader is \, but a comma is much better
  set backspace=indent,eol,start                                    " Make backspace behave like every other editor
  set expandtab                                                     " Convert tab to spaces if <Tab> is pressed
  set tabstop=4                                                     " Number of spaces used to replace a tab
  set shiftwidth=4                                                  " Number of spaces for indentation
  set softtabstop=4                                                 " Number of spaces to delete wenn pressing <DELETE>

" }}}


" Visuals {{{

  set background=dark                                               " Change default background color
  colorscheme onehalfdark                                           " Change colorscheme
  set number                                                        " Enable line numbers
  set cursorline                                                    " Highlight line of the cursor

" }}}


" Section Folding {{{

  set foldenable
  set foldlevelstart=10
  set foldnestmax=10
  set foldmethod=syntax
  nnoremap <space> za
  nnoremap <expr> <f2> &foldlevel ? 'zM' :'zR'                      " Toggle folding everything

" }}}


" Plugin Settings {{{

  " Lightline {{{
 
    let g:lightline = { 'colorscheme': 'onehalfdark' }              " Set colorscheme for lightline
    set laststatus=2                                                " Show statusbar
    set noshowmode                                                  " Hide default modes
    function! LightlineReload()                                     " Function to reload lightline
      call lightline#init()
      call lightline#colorscheme()
      call lightline#update()
    endfunction

  " }}}

  " Search {{{

    set hlsearch                                                    " Highlight all matched terms.
    set incsearch                                                   " Incrementally highlight, as we type.
    nmap <Leader><space> :nohlsearch<cr>                            " Remove search highlight.

  " }}}

  " Split Management {{{

    set splitbelow                                                  " Make splits default to below
    set splitright                                                  " And to the right. This feels more natural
    nmap <C-J> <C-W><C-J>
    nmap <C-K> <C-W><C-K>
    nmap <C-H> <C-W><C-H>
    nmap <C-L> <C-W><C-L>

  " }}}

  " NERDTree {{{
 
    let NERDTreeHijackNetrw = 0
    nmap <A-1> :NERDTreeToggle<cr>
    autocmd StdinReadPre * let s:std_in=1
    autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif " Autostart NERDTree if no files where specified
    autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | exe 'cd '.argv()[0] | endif " Autostart NERDTree if open directory
    autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif " Close vim if only NERDTree is opened

  " }}}


  " fzf {{{
 
    nmap ; :Buffers<CR>
    nmap <Leader>f :Files<CR>
    nmap <Leader>t :Tags<CR>

  " }}}

" }}}


" Autosource vim config {{{

  augroup autosourcing
    autocmd!
    autocmd BufWritePost ~/.config/nvim/init.vim source %
    autocmd BufWritePost ~/.config/nvim/init.vim call LightlineReload()
    autocmd BufWritePost ~/.config/nvim/init.vim doautocmd BufRead
  augroup END
  nmap <Leader>ev :tabedit $MYVIMRC<cr> " quickly edit vim config

" }}}


" Enable True Colors {{{

  if (has("nvim"))
    "For Neovim 0.1.3 and 0.1.4 < https://github.com/neovim/neovim/pull/2198 >
    let $NVIM_TUI_ENABLE_TRUE_COLOR=1
    " Escape inside a FZF terminal window should exit the terminal window
    " rather than going into the terminal's normal mode.
    autocmd FileType fzf tnoremap <buffer> <Esc> <Esc>
  endif
  
  "For Neovim > 0.1.5 and Vim > patch 7.4.1799 < https://github.com/vim/vim/commit/61be73bb0f965a895bfb064ea3e55476ac175162 >
  "Based on Vim patch 7.4.1770 (`guicolors` option) < https://github.com/vim/vim/commit/8a633e3427b47286869aa4b96f2bfc1fe65b25cd >
  " < https://github.com/neovim/neovim/wiki/Following-HEAD#20160511 >
  if (has("termguicolors"))
    set termguicolors
  endif

" }}}

" vim:foldmethod=marker:foldlevel=0
