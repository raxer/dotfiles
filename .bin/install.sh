DOTFILES_DIR=.dotfiles
DOTFILES_BACKUP_DIR=.dotfiles-backup
GIT_REPO=git@gitlab.com:raxer/dotfiles.git

git clone --bare $GIT_REPO $HOME/$DOTFILES_DIR

function config {
  /usr/bin/git --git-dir=$HOME/$DOTFILES_DIR/ --work-tree=$HOME $@
}

mkdir -p $DOTFILES_BACKUP_DIR
config checkout
if [ $? = 0 ]; then
  echo "Checked out config.";
else
  echo "Backing up pre-existing dot files.";
  config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} mv {} $DOTFILES_BACKUP_DIR/{}
fi;

config checkout
config config status.showUntrackedFiles no
